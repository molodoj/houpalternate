package space.moonstudio.houpalternate.util;

import me.lucko.luckperms.LuckPerms;
import me.lucko.luckperms.api.Group;
import me.lucko.luckperms.api.LuckPermsApi;
import me.lucko.luckperms.api.User;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.permission.Permission;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

import java.util.Comparator;

import java.util.Comparator;

public class PluginsUtils {

    private static RegisteredServiceProvider<Chat> rsp = Bukkit.getServicesManager().getRegistration(Chat.class);
    private static RegisteredServiceProvider<Permission> permissionProvider = Bukkit.getServicesManager().getRegistration(Permission.class);
    private static Chat chat = rsp.getProvider();
    private static Permission permission = permissionProvider.getProvider();


    public static String getPrefix(CommandSender sender) {
        try {
            if (sender instanceof Player) {
                Player player = ((Player) sender);
                return StringUtils.trimToEmpty(chat.getPlayerPrefix(player));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getSuffix(CommandSender sender) {
        try {
            if (sender instanceof Player) {
                Player player = ((Player) sender);
                return StringUtils.trimToEmpty(chat.getPlayerSuffix(player));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getPrimaryGroup(CommandSender sender) {
        try {
            if (sender instanceof Player) {
                Player player = ((Player) sender);

                LuckPermsApi luckPerms = LuckPerms.getApi();
                User user = luckPerms.getUserManager().getUser(player.getUniqueId());
                if (user == null) {
                    return "default";
                }
                return luckPerms.getGroupManager().getLoadedGroups().stream()
                        .filter(group -> sender.hasPermission("group." + group.getName()))
                        .max(Comparator.comparingInt(value -> value.getWeight().orElse(0)))
                        .map(Group::getName)
                        .orElse("default");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "default";
    }
}
