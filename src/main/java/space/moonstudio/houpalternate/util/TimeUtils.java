package space.moonstudio.houpalternate.util;

import space.moonstudio.houpalternate.Message;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class TimeUtils {

    private static final int maxYears = 10;

    private static String[] year;
    private static String[] month;
    private static String[] day;

    private static String[] hour;
    private static String[] minute;


    public static void init() {
        year = new String[]{
                Message.ГОД.get(),
                Message.ГОДА.get(),
                Message.ЛЕТ.get()};
        month = new String[]{
                Message.МЕСЯЦ.get(),
                Message.МЕСЯЦА.get(),
                Message.МЕСЯЦЕВ.get()};
        day = new String[]{
                Message.ДЕНЬ.get(),
                Message.ДНЯ.get(),
                Message.ДНЕЙ.get()};
        hour = new String[]{
                Message.ЧАС.get(),
                Message.ЧАСА.get(),
                Message.ЧАСОВ.get()};
        minute = new String[]{
                Message.МИНУТА.get(),
                Message.МИНУТЫ.get(),
                Message.МИНУТ.get()};
        second = new String[]{
                Message.СЕКУНДА.get(),
                Message.СЕКУНДЫ.get(),
                Message.СЕКУНД.get()};
    }

    private static String[] second;


    private static int dateDiff(int type, Calendar fromDate, Calendar toDate, boolean future) {
        int year = Calendar.YEAR;

        int fromYear = fromDate.get(year);
        int toYear = toDate.get(year);
        if (Math.abs(fromYear - toYear) > maxYears) {
            toDate.set(year, fromYear +
                    (future ? maxYears : -maxYears));
        }

        int diff = 0;
        long savedDate = fromDate.getTimeInMillis();
        while ((future && !fromDate.after(toDate)) || (!future && !fromDate.before(toDate))) {
            savedDate = fromDate.getTimeInMillis();
            fromDate.add(type, future ? 1 : -1);
            diff++;
        }
        diff--;
        fromDate.setTimeInMillis(savedDate);
        return diff;
    }

    private static int[] allTimeTypes = new int[]{
            Calendar.YEAR,
            Calendar.MONTH,
            Calendar.DAY_OF_MONTH,
            Calendar.HOUR_OF_DAY,
            Calendar.MINUTE,
            Calendar.SECOND};

    public static String formatDateDiff(long seconds) {
        return formatDateDiff(seconds, allTimeTypes);
    }

    /**
     * Преобразовать строку в русское описание времени
     *
     * @param seconds   секунды
     * @param timeTypes типы времени:
     *                  <ul>
     *                  <li>{@link Calendar#YEAR}</li>
     *                  <li>{@link Calendar#MONTH}</li>
     *                  <li>{@link Calendar#DAY_OF_MONTH}</li>
     *                  <li>{@link Calendar#HOUR_OF_DAY}</li>
     *                  <li>{@link Calendar#MINUTE}</li>
     *                  <li>{@link Calendar#SECOND}</li>
     *                  </ul>
     * @return время в виде строки
     */
    public static String formatDateDiff(long seconds, int... timeTypes) {
        Calendar c = new GregorianCalendar();
        c.setTimeInMillis(System.currentTimeMillis() + seconds * 1000);
        Calendar now = new GregorianCalendar();
        return TimeUtils.formatDateDiff(now, c, timeTypes);
    }

    private static String formatDateDiff(Calendar fromDate, Calendar toDate, int... timeTypes) {
        boolean future = false;
        if (toDate.equals(fromDate)) {
            return "0 " + second[2];
        }
        if (toDate.after(fromDate)) {
            future = true;
        }
        StringBuilder sb = new StringBuilder();
//        int accuracy = 0;
        for (int type : timeTypes) {
//            if (accuracy > 2) {
//                break;
//            }
            int diff = dateDiff(type, fromDate, toDate, future);
            if (diff > 0) {
//                accuracy++;
                sb.append(" ").append(diff).append(" ").append(getName(type, diff));
            }
        }
        if (sb.length() == 0) {
            return "0 " + second[2];
        }
        return sb.toString().trim();
    }

    private static String getName(int type, int diff) {
        String diff_str = String.valueOf(diff);
        switch (type) {
            case Calendar.YEAR:
                return getName(year, diff_str);
            case Calendar.MONTH:
                return getName(month, diff_str);
            case Calendar.DAY_OF_MONTH:
            case Calendar.DAY_OF_WEEK:
            case Calendar.DAY_OF_WEEK_IN_MONTH:
            case Calendar.DAY_OF_YEAR:
                return getName(day, diff_str);
            case Calendar.HOUR_OF_DAY:
            case Calendar.HOUR:
                return getName(hour, diff_str);
            case Calendar.MINUTE:
                return getName(minute, diff_str);
            case Calendar.SECOND:
                return getName(second, diff_str);
            default:
                return null;
        }
    }

    private static String getName(String[] name, String diff_str) {
        if (diff_str.endsWith("0") || diff_str.matches("[2-9][5-9]$") || diff_str.matches("1[1-9]$") || diff_str.matches("[5-9]$")) {
            return name[2];
        }
        if (diff_str.endsWith("1")) {
            return name[0];
        }
        return name[1];
    }
}
