package space.moonstudio.houpalternate.util;

import com.google.gson.*;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import lombok.SneakyThrows;
import net.minecraft.server.v1_13_R2.LocaleLanguage;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.yaml.snakeyaml.Yaml;
import space.moonstudio.houpalternate.Main;
import space.moonstudio.houpalternate.MyObject;
import space.moonstudio.houpalternate.Try;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.*;

public class ItemUtils {

    private static  ItemFlag[] itemFlags     = ItemFlag.values();

    private static Gson gson = new GsonBuilder()
            .registerTypeAdapter(new TypeToken<Map <String, Object>>(){}.getType(),  new MapDeserializerDoubleAsIntFix())
            .create();
    private static YamlConfiguration config = new YamlConfiguration();
    private static Yaml yaml = new Yaml();
    static {
    }

    public static String itemToString(ItemStack item) {
        config.set("item", item);
        String yamlText = config.saveToString();
        Map map = yaml.loadAs(yamlText, Map.class);
        return gson.toJson(map);
    }


    public static ItemStack stringToItem(String json) {
        try {
            Map map = gson.fromJson(json, new TypeToken<Map<String, Object>>(){}.getType());
            String yamlText = yaml.dump(map);
            config.loadFromString(yamlText);
            return (ItemStack) config.get("item");
        } catch (Exception e) {
            throw new RuntimeException("ошибка парсинга предмета " + json, e);
        }
    }

    public static class MapDeserializerDoubleAsIntFix implements JsonDeserializer<Map<String, Object>> {

        @Override  @SuppressWarnings("unchecked")
        public Map<String, Object> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            return (Map<String, Object>) read(json);
        }

        public Object read(JsonElement in) {

            if(in.isJsonArray()){
                List<Object> list = new ArrayList<Object>();
                JsonArray arr = in.getAsJsonArray();
                for (JsonElement anArr : arr) {
                    list.add(read(anArr));
                }
                return list;
            }else if(in.isJsonObject()){
                Map<String, Object> map = new LinkedTreeMap<String, Object>();
                JsonObject obj = in.getAsJsonObject();
                Set<Map.Entry<String, JsonElement>> entitySet = obj.entrySet();
                for(Map.Entry<String, JsonElement> entry: entitySet){
                    map.put(entry.getKey(), read(entry.getValue()));
                }
                return map;
            }else if( in.isJsonPrimitive()){
                JsonPrimitive prim = in.getAsJsonPrimitive();
                if(prim.isBoolean()){
                    return prim.getAsBoolean();
                }else if(prim.isString()){
                    return prim.getAsString();
                }else if(prim.isNumber()){

                    Number num = prim.getAsNumber();
                    // here you can handle double int/long values
                    // and return any type you want
                    // this solution will transform 3.0 float to long values
                    if(Math.ceil(num.doubleValue())  == num.longValue())
                        return num.longValue();
                    else{
                        return num.doubleValue();
                    }
                }
            }
            return null;
        }
    }

    public static ItemStack setText(ItemStack item, String text) {
        if(item == null) {
            return null;
        }
        if(text != null) {
            String[] data = text.split("(::|\n|\r)");
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(data[0]);
            meta.setLore(data.length > 1 ? Arrays.asList(data).subList(1, data.length) : null);
            item.setItemMeta(meta);
        } else {
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(null);
            meta.setLore(null);
            item.setItemMeta(meta);
        }
        return item;
    }

    public static void giveItemsOrDrops(Player player, List<ItemStack> items) {
        PlayerInventory inventory = player.getInventory();
        HashMap<Integer, ItemStack> map = inventory.addItem(items.toArray(new ItemStack[0]));
        map.forEach((count, item) -> {
            if (item != null && item.getAmount() > 0 && !item.getType().equals(Material.AIR)) {
                Location location = player.getLocation();
                player.getWorld().dropItem(location, item);
            }
        });
    }

    public static boolean isNullOrAir(ItemStack item) {
        return item == null || item.getType().equals(Material.AIR);
    }

    public static ItemStack hideInfo(ItemStack item) {
        ItemMeta meta = item.getItemMeta();
        meta.removeItemFlags(ItemFlag.values());
        meta.addItemFlags(itemFlags);
        item.setItemMeta(meta);
        return item;
    }

    private static MyObject myLocaleLanguage = MyObject.wrap(LocaleLanguage.class).getField("c");
    private static LocaleLanguage localeLanguage = myLocaleLanguage.getObject();
    private static Field LocaleLanguage_d;
    static {
        Try.ignore(() -> {
            LocaleLanguage_d = LocaleLanguage.class.getDeclaredField("d");
            LocaleLanguage_d.setAccessible(true);
        });
    }
    private static Map<String, String> defaultLocale = myLocaleLanguage.getField("d").getObject();

    @SneakyThrows
    public static String getMinecraftName(ItemStack item) {
        LocaleLanguage_d.set(localeLanguage, Main.getInstance().getLocale());
        String text = CraftItemStack.asNMSCopy(item).getName().getText();
        LocaleLanguage_d.set(localeLanguage, defaultLocale);
        return text;
    }
}
