package space.moonstudio.houpalternate.util;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.ChatColor;

public class MessageUtils {

    public static String colored(String text) {
        if (text == null || text.isEmpty()) {
            return text;
        }
        text = ChatColor.translateAlternateColorCodes('&', text);
        text = StringUtils.replace(text, "\\n", "\n");
        text = StringUtils.remove(text, '\r');
        return text;

    }

    public static String colored(String text, String... replaced) {
        return colored(replaced(text, replaced));
    }

    public static String replaced(String text, String... replaced) {
        for (int i = 1; i < replaced.length; i += 2) {
            text = StringUtils.replace(text, replaced[i - 1], replaced[i]);
        }
        return text;
    }
}
