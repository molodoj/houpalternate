package space.moonstudio.houpalternate.command;

import net.minecraft.server.v1_13_R2.*;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_13_R2.CraftWorld;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftInventoryPlayer;
import org.bukkit.entity.Player;
import space.moonstudio.houpalternate.Message;
import space.moonstudio.houpalternate.Metadata;

public class AnvilCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Only for player.");
            return false;
        }

        if (!(sender.hasPermission("ha.anvil") || sender.hasPermission("ha.admin"))) {
            sender.sendMessage(Message.NO_PERMISSION_ANVIL.get());
            return false;
        }

        Player player = (Player) sender;
        Metadata metadata = Metadata.get(player);
        EntityPlayer handle = ((CraftPlayer) player).getHandle();
        ContainerAnvil containerAnvil = metadata.getContainerAnvil();
        if (containerAnvil != null) {
            int id = handle.nextContainerCounter();
            handle.activeContainer = containerAnvil;
            handle.playerConnection.sendPacket(new PacketPlayOutOpenWindow(id, "minecraft:anvil", new ChatMessage(Blocks.ANVIL.m())));
            handle.activeContainer.windowId = id;
        } else {
            Location loc = player.getLocation();
            BlockPosition position = new BlockPosition(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
            WorldServer world = ((CraftWorld) player.getWorld()).getHandle();
            containerAnvil = new ContainerAnvil(((CraftInventoryPlayer) player.getInventory()).getInventory(), world, position, handle) {
                @Override
                public boolean canUse(EntityHuman entityhuman) {
                    return true;
                }
            };
            int id = handle.nextContainerCounter();
            handle.activeContainer = containerAnvil;
            handle.playerConnection.sendPacket(new PacketPlayOutOpenWindow(id, "minecraft:anvil", new ChatMessage(Blocks.ANVIL.m())));
            handle.activeContainer.windowId = id;
            handle.activeContainer.addSlotListener(handle);
            metadata.setContainerAnvil(containerAnvil);
        }
        return false;
    }
}
