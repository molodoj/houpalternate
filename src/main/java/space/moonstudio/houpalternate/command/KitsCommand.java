package space.moonstudio.houpalternate.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import space.moonstudio.houpalternate.Message;
import space.moonstudio.houpalternate.Metadata;
import space.moonstudio.houpalternate.kit.KitManager;
import space.moonstudio.houpalternate.util.ItemUtils;

public class KitsCommand implements CommandExecutor {
    private KitManager kitManager;

    public KitsCommand(KitManager kitManager) {
        this.kitManager = kitManager;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Only for player.");
            return false;
        }

        if (!(sender.hasPermission("ha.kits") || sender.hasPermission("ha.admin"))) {
            sender.sendMessage(Message.NO_PERMISSION_KITS.get());
            return false;
        }

        if (!kitManager.isEnable()) {
            sender.sendMessage(Message.KIT_SYSTEM_DISABLED.get());
            return false;
        }

        Player player = (Player) sender;
        Metadata metadata = Metadata.get(player);
        Inventory kitsGui = metadata.getKitsGui();
        if (kitsGui == null) {
            kitsGui = Bukkit.createInventory(null, 9 * 6, Message.KIT_GUI_NAME.get());

            kitsGui.setItem(kitManager.getBottleSlot(), ItemUtils.setText(kitManager.getBottleItem().clone(), Message.GUI_KITS_BOTTLE_TEXT.get()));
            kitsGui.setItem(kitManager.getBackSlot(), ItemUtils.setText(kitManager.getBackItem().clone(), Message.KIT_GUI_BACK_TEXT.get()));

            kitManager.updateGuiKitItems(player, kitsGui);

            metadata.setKitsGui(kitsGui);
        }
        player.openInventory(kitsGui);
        return true;
    }
}
