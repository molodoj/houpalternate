package space.moonstudio.houpalternate.command;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import space.moonstudio.houpalternate.Main;
import space.moonstudio.houpalternate.Message;
import space.moonstudio.houpalternate.util.CommandUtils;
import space.moonstudio.houpalternate.util.ItemUtils;

public class AdminHoupAlternateCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("ha.admin")) {
            sender.sendMessage(Message.NO_PERMISSION_ADMIN.get());
            return false;
        }

        if (args.length == 0 || args[0].equalsIgnoreCase("help")) {
            sender.sendMessage(Message.HOUPMINE_HELP.get());
            return true;
        }

        if (args[0].equalsIgnoreCase("reload")) {
            Main.getInstance().reloadCustomConfig();
            sender.sendMessage(Message.CONFIG_RELOAD.get());
            return true;
        }

        if (args[0].equalsIgnoreCase("item")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("only for player");
                return false;
            }

            Player player = (Player) sender;

            ItemStack item = player.getInventory().getItemInMainHand();
            if (item == null || item.getType().equals(Material.AIR)) {
                sender.sendMessage("§cВозьмите предмет в руку.");
                return false;
            }
            String text = ItemUtils.itemToString(item);
            sender.sendMessage(text);
            Main.getInstance().getLogger().info("Текст предмета: " + text);
            sender.sendMessage("§aТекст предмета для конфига вам был отправлен выше, а так же он был загголирован в консоль сервера.");
            return true;
        }

        if (args[0].equalsIgnoreCase("fromstring")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("only for player");
                return false;
            }

            Player player = (Player) sender;

            if (args.length < 2) {
                sender.sendMessage("§cУкажите текст предмета.");
                return false;
            }

            try {
                String text = CommandUtils.getArguments(1, args);
                ItemStack item = ItemUtils.stringToItem(text);
                player.getInventory().addItem(item);
                sender.sendMessage("§aПредмен был выдан вам в инвентарь.");
                return true;
            } catch (Exception e) {
                sender.sendMessage("§cПроизошла ошибка при попытке получения предмета, подробнее в консолии.");
                e.printStackTrace();
                return false;
            }
        }

        sender.sendMessage(Message.HOUPMINE_ARGUMENT_NOT_FOUND.get());
        return false;
    }
}
