package space.moonstudio.houpalternate.command;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import space.moonstudio.houpalternate.Main;
import space.moonstudio.houpalternate.Message;
import space.moonstudio.houpalternate.Metadata;

import java.util.Map;
import java.util.concurrent.TimeUnit;

public class DispellCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Only for player.");
            return false;
        }

        if (!(sender.hasPermission("ha.dispell") || sender.hasPermission("ha.admin"))) {
            sender.sendMessage(Message.NO_PERMISSION_DISPELL.get());
            return false;
        }

        Player player = (Player) sender;
        ItemStack mainHand = player.getInventory().getItemInMainHand();
        if (mainHand.getType().equals(Material.AIR)) {
            player.sendMessage(Message.NO_ITEM.get());
            return false;
        }

        Map<Enchantment, Integer> enchantments = mainHand.getEnchantments();
        if (enchantments.isEmpty()) {
            player.sendMessage(Message.NO_ENCHANTED.get());
            return false;
        }

        Metadata metadata = Metadata.get(player);
        long now = System.currentTimeMillis();

        if (metadata.getConfirmItem() == null || !metadata.getConfirmItem().isSimilar(mainHand)) {
            metadata.setStartConfirm(now);
            metadata.setConfirmItem(mainHand);
            player.sendMessage(Message.CONFIRM.get());
            return true;
        }

        if (now - metadata.getStartConfirm() > TimeUnit.SECONDS.toMillis(Main.getInstance().getTimeSeconds())) {
            metadata.setStartConfirm(now);
            metadata.setConfirmItem(mainHand);
            player.sendMessage(Message.CONFIRM.get());
            return true;
        }

        enchantments.keySet().forEach(mainHand::removeEnchantment);
        player.getInventory().setItemInMainHand(mainHand);
        metadata.setStartConfirm(0);
        metadata.setConfirmItem(null);
        player.sendMessage(Message.DISPELL.get());
        return false;
    }
}
