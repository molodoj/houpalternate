package space.moonstudio.houpalternate.command;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import space.moonstudio.houpalternate.Message;
import space.moonstudio.houpalternate.kit.Kit;
import space.moonstudio.houpalternate.kit.KitManager;
import space.moonstudio.houpalternate.util.TimeUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class KitCommand implements CommandExecutor, TabExecutor {
    private KitManager kitManager;

    public KitCommand(KitManager kitManager) {
        this.kitManager = kitManager;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Only for player.");
            return false;
        }

        if (!(sender.hasPermission("ha.kit") || sender.hasPermission("ha.admin"))) {
            sender.sendMessage(Message.NO_PERMISSION_KITS.get());
            return false;
        }

        if (!kitManager.isEnable()) {
            sender.sendMessage(Message.KIT_SYSTEM_DISABLED.get());
            return false;
        }

        if (args.length < 1) {
            String sep = Message.ENTER_KIT_NAME_SEPARATOR.get();
            sender.sendMessage(Message.ENTER_KIT_NAME.get("{kits}", kitManager.getKits().stream().map(Kit::getName).collect(Collectors.joining(sep))));
            return false;
        }

        Player player = (Player) sender;

        String kitName = args[0];
        Kit kit = kitManager.getKits().stream()
                .filter(it -> it.getName().equalsIgnoreCase(kitName))
                .findFirst().orElse(null);

        if (kit == null) {
            String sep = Message.ENTER_KIT_NAME_SEPARATOR.get();
            sender.sendMessage(Message.ENTER_KIT_NAME.get("{kits}", kitManager.getKits().stream().map(Kit::getName).collect(Collectors.joining(sep))));
            return false;
        }

        long nextUse = kitManager.getNextUse(kit, player);
        long current = System.currentTimeMillis();
        if (nextUse > current) {
            int waitSeconds = (int) ((nextUse - current) / 1000);
            sender.sendMessage(Message.KIT_COMMAND_DISABLE.get("{time}", TimeUtils.formatDateDiff(waitSeconds)));
            return false;
        }

        kitManager.performGiveKit(player, kit);
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] args) {
        if (args.length == 1) {
            return kitManager.getKits()
                    .stream()
                    .filter(kit -> StringUtils.startsWithIgnoreCase(kit.getName(), args[0]))
                    .map(Kit::getName)
                    .collect(Collectors.toList());

        }
        return Collections.emptyList();
    }
}
