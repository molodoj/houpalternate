package space.moonstudio.houpalternate.command;

import lombok.*;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import space.moonstudio.houpalternate.Config;
import space.moonstudio.houpalternate.Message;
import space.moonstudio.houpalternate.util.MessageUtils;
import space.moonstudio.houpalternate.util.PluginsUtils;

import java.util.*;
import java.util.stream.Collectors;

public class ListCommand implements CommandExecutor {

    private String header;
    private String formatPlayer;
    private String separator;
    private String empty;
    private List<Category> categories;

    @SuppressWarnings("unchecked")
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (!(sender.hasPermission("ha.list") || sender.hasPermission("ha.admin"))) {
            sender.sendMessage(Message.NO_PERMISSION_LIST.get());
            return false;
        }

        Collection<Player> players = (Collection<Player>) Bukkit.getOnlinePlayers();

        sender.sendMessage(header.replace("{online}", String.valueOf(players.size())));

        List<PlayerInfo> infos = players.stream()
                .map(player -> {
                    String primaryGroup = PluginsUtils.getPrimaryGroup(player);
                    Category category = null;
                    for (Category it : categories) {
                        if (category != null && category.getPriority() >= it.getPriority()) {
                            continue;
                        }
                        if (it.getGroups().contains(primaryGroup)) {
                            category = it;
                        }
                    }

                    return new PlayerInfo(player,
                            MessageUtils.colored(PluginsUtils.getPrefix(player)),
                            MessageUtils.colored(PluginsUtils.getSuffix(player)),
                            category);
                })
                .collect(Collectors.toList());

        for (Category category : categories) {
            String playerList = infos.stream()
                    .filter(player -> Objects.equals(category, player.getCategory()))
                    .map(player -> formatPlayer
                            .replace("{name}", player.getPlayer().getName())
                            .replace("{prefix}", player.getPrefix())
                            .replace("{suffix}", player.getSuffix()))
                    .collect(Collectors.joining(separator));
            if (playerList.isEmpty()) {
                playerList = empty;
            }
            String text = category.getFormat()
                    .replace("{list}", playerList);
            sender.sendMessage(text);
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    public void reloadConfig(Config customConfig) {
        header = MessageUtils.colored(customConfig.getOrSet("list.header", "&aОнлайн сервера {online}:"));
        formatPlayer = MessageUtils.colored(customConfig.getOrSet("list.format-player", "{prefix}{name}{suffix}"));
        separator = MessageUtils.colored(customConfig.getOrSet("list.separator", "&7, "));
        empty = MessageUtils.colored(customConfig.getOrSet("list.empty", "§8Никого нет"));
        categories = new ArrayList<>();
        List<Map> categoriesData = (List) customConfig.get("list.categories");
        if (categoriesData == null) {
            categoriesData = new ArrayList<>(3);

            Map<String, Object> category = new HashMap<>();
            category.put("groups", Arrays.asList("vip", "gold", "premium"));
            category.put("format", "Донатеры: {list}");
            category.put("priority", 1);
            categoriesData.add(category);


            category = new HashMap<>();
            category.put("groups", Collections.singletonList("default"));
            category.put("format", "Игроки: {list}");
            category.put("priority", 0);
            categoriesData.add(category);

            category = new HashMap<>();
            category.put("groups", Arrays.asList("builder", "staff", "admin"));
            category.put("format", "Админы: {list}");
            category.put("priority", 2);
            categoriesData.add(category);
            customConfig.setAndSave("list.categories", categoriesData);
        }

        for (Map categoryData : categoriesData) {
            try {
                Category category = new Category(
                        (List)categoryData.get("groups"),
                        MessageUtils.colored((String) categoryData.get("format")),
                        ((Number)categoryData.get("priority")).intValue()
                );
                categories.add(category);
            } catch (Exception e) {
                Bukkit.getLogger().info("error load category " + categoryData);
                e.printStackTrace();
            }
        }
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @ToString
    public static class Category {
        private List<String> groups;
        private String format;
        private int priority;
    }

    @Data
    @AllArgsConstructor
    public static class PlayerInfo {
        private Player player;
        private String prefix;
        private String suffix;
        private Category category;
    }
}
