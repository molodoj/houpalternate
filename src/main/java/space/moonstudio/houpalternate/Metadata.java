package space.moonstudio.houpalternate;

import lombok.Data;
import lombok.Getter;
import net.minecraft.server.v1_13_R2.ContainerAnvil;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Data
public class Metadata {
    @Getter
    private static Map<Player, Metadata> players = new ConcurrentHashMap<>();

    private final Player player;

    private ContainerAnvil containerAnvil;
    private long startConfirm;
    private ItemStack confirmItem;

    private Inventory kitsGui;

    public Metadata(Player player) {
        this.player = player;
    }

    public static Metadata get(Player player) {
        return players.computeIfAbsent(player, Metadata::new);
    }
}
