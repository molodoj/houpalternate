package space.moonstudio.houpalternate.invcleaner;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import space.moonstudio.houpalternate.Config;
import space.moonstudio.houpalternate.Message;

import java.util.LinkedList;
import java.util.List;

public class InvCleanerManager implements Listener {

    private List<Material> items = new LinkedList<>();

    public InvCleanerManager(JavaPlugin plugin, Config config) {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);

        List<String> itemsList = config.getStringList("item-blacklist");

        for (String item : itemsList) {
            try {
                Material material = toMaterial(item);
                items.add(material);
            } catch (Exception e) {
                System.out.print("Произошла ошибка при парсинге предмета!");
                System.out.print("Item: " + item);
                e.printStackTrace();
            }
        }

        plugin.getCommand("houpclearinventory").setExecutor((sender, command, label, args) -> {
            Player player = (Player) sender;
            if (args.length == 0) {
                if (!player.hasPermission("houpalternate.invcleaner")) {
                    Message.INVCLEANER_NO_PERMISSION.prefix().send(player);
                    return false;
                }
                clear(player);
                Message.INVCLEANER_SUCCESS.prefix().send(player);
                return true;
            }
            if (args.length == 1) {
                if (!player.hasPermission("houpalternate.invcleaner.other")) {
                    Message.INVCLEANER_NO_PERMISSION_OTHER.prefix().send(player);
                    return false;
                }
                Player remove = Bukkit.getPlayer(args[0]);
                if (remove != null) {
                    clear(remove);
                    Message.INVCLEANER_SUCCESS_OTHER.prefix().send(player);
                    return true;
                } else {
                    Message.INVCLEANER_TARGET_OFFLINE.prefix().send(player);
                    return false;
                }
            }
            if (args.length > 2) {
                Message.INVCLEANER_USAGE.prefix().send(player);
                return false;
            }
            return false;
        });
    }

    private void clear(Player player) {
        for (ItemStack itemStack : player.getInventory().getContents()) {
            if (itemStack == null) continue;
            if (!items.contains(itemStack.getType())) {
                player.getInventory().remove(itemStack);
            }
        }
        ItemStack[] armor = player.getInventory().getArmorContents();
        for (int i = 0; i < armor.length; i++) {
            if (armor[i] == null) continue;
            if (!items.contains(armor[i].getType())) {
                armor[i] = null;
            }
        }
        player.getInventory().setArmorContents(armor);
        if (!items.contains(player.getInventory().getItemInOffHand().getType())) {
            player.getInventory().setItemInOffHand(null);
        }
    }

    private Material toMaterial(String code) {
        if (code == null) {
            return null;
        }
        try {
            return Material.getMaterial(code.toUpperCase());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
