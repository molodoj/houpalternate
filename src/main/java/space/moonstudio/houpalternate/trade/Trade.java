package space.moonstudio.houpalternate.trade;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import space.moonstudio.houpalternate.Message;

import java.util.Objects;
import java.util.Set;

// hardcoded af
public class Trade implements Listener {

    private Inventory inventory;
    private boolean targetReady, playerReady, end;
    private Player target, player;
    private Set<Integer> targetSlots, playerSlots;
    private ItemStack readyItem, notReadyItem;

    public Trade(Plugin plugin, Player player, Player target, Set<Integer> targetSlots, Set<Integer> playerSlots, ItemStack readyItem, ItemStack notReadyItem) {
        this.targetSlots = targetSlots;
        this.playerSlots = playerSlots;
        this.readyItem = readyItem;
        this.notReadyItem = notReadyItem;
        Bukkit.getServer().getPluginManager().registerEvents(this, plugin);

        this.player = player;
        this.target = target;

        player.closeInventory();
        target.closeInventory();

        createInventory();

        new BukkitRunnable() {
            public void run() {
                target.openInventory(inventory);
                player.openInventory(inventory);
            }
        }.runTaskLater(plugin, 5L);
    }

    private void close() {
        this.end = true;
        processItems(targetReady && playerReady);

        target.closeInventory();
        player.closeInventory();
        HandlerList.unregisterAll(this);
    }

    private void processItems(boolean trade) {
        targetSlots.stream()
                   .map(slot -> inventory.getItem(slot))
                   .filter(Objects::nonNull)
                   .filter(item -> item.getType() != Material.AIR)
                   .forEach(item -> {
                       if (trade) {
                           player.getInventory().addItem(item);
                       } else {
                           target.getInventory().addItem(item);
                       }
                   });

        playerSlots.stream()
                   .map(slot -> inventory.getItem(slot))
                   .filter(Objects::nonNull)
                   .filter(item -> item.getType() != Material.AIR)
                   .forEach(item -> {
                       if (trade) {
                           target.getInventory().addItem(item);
                       } else {
                           player.getInventory().addItem(item);
                       }
                   });
    }

    private void createInventory() {
        String title = Message.TRADE_INVENTORY_TITLE.replaced("{target}", target.getName(), "{player}", player.getName()).get();
        this.inventory = Bukkit.createInventory(null, 54, title);

        ItemStack item = new ItemStack(Material.BLACK_STAINED_GLASS_PANE, 1);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("");
        item.setItemMeta(meta);
        int[] background = {4, 13, 22, 31, 40, 49, 36, 37, 38, 39, 41, 42, 43, 44, 46, 47, 48, 49, 50, 51, 52};
        for (int slot : background) {
            inventory.setItem(slot, item);
        }
        update();
    }

    private void update() {
        if (targetReady && playerReady) {
            close();
            return;
        }
        inventory.setItem(45, targetReady ? readyItem : notReadyItem);
        inventory.setItem(53, playerReady ? readyItem : notReadyItem);
    }

    private boolean moveFrom(InventoryClickEvent event) {
        int rawSlot = -1;
        if (event.getRawSlot() > 80) {
            rawSlot = event.getRawSlot() - 81;
        } else if (event.getRawSlot() > 53) {
            rawSlot = event.getRawSlot() - 45;
        }
        if (rawSlot == -1) {
            return false;
        }
        Player player = (Player) event.getWhoClicked();
        if (player.getInventory().getItem(rawSlot) == null) {
            return false;
        }
        Set<Integer> slots = player == target ? targetSlots : playerSlots;
        for (int slot : slots) {
            if (event.getInventory().getItem(slot) == null) {
                inventory.setItem(slot, player.getInventory().getItem(rawSlot));
                player.getInventory().setItem(rawSlot, null);
                return true;
            }
        }
        return false;
    }

    private void moveTo(InventoryClickEvent event) {
        Player whoClicked = (Player) event.getWhoClicked();

        Set<Integer> slots = whoClicked == target ? targetSlots : playerSlots;
        if (!slots.contains(event.getRawSlot())) {
            return;
        }
        if (inventory.getItem(event.getRawSlot()) != null) {
            whoClicked.getInventory().addItem(inventory.getItem(event.getRawSlot()));
            inventory.setItem(event.getRawSlot(), null);
        }
    }

    @EventHandler
    public void onCloseInventory(InventoryCloseEvent e) {
        if (!end && e.getInventory().equals(inventory)) {
            close();
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if (!event.getInventory().equals(inventory)) {
            return;
        }
        event.setCancelled(true);

        int slot = event.getRawSlot();
        if (slot < 0) {
            return;
        }
        if (slot > 53) {
            moveFrom(event);
        } else if (slot != 45 && (slot != 53)) {
            moveTo(event);
        } else {
            Player whoClicked = (Player) event.getWhoClicked();
            if (slot == 45) {
                if (whoClicked == target) {
                    this.targetReady = !targetReady;
                }
            } else if (whoClicked == player) {
                this.playerReady = !playerReady;
            }
            update();
            return;
        }

        this.targetReady = false;
        this.playerReady = false;
        update();
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        if ((e.getPlayer() == target) || (e.getPlayer() == player)) {
            close();
        }
    }
}
