package space.moonstudio.houpalternate.trade;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import space.moonstudio.houpalternate.Message;

import java.util.*;

import static org.bukkit.Bukkit.getServer;

// hardcoded af
public class TradeManager implements Listener {

    private final Plugin plugin;

    private Map<Player, Player> requests = new HashMap<>();

    private ItemStack readyItem;
    private ItemStack notReadyItem;
    private HashSet<Integer> targetSlots;
    private HashSet<Integer> playerSlots;

    public TradeManager(JavaPlugin plugin) {
        this.plugin = plugin;

        createItems();

        getServer().getPluginManager().registerEvents(this, plugin);
        plugin.getCommand("trade").setExecutor(this::onCommand);
    }

    private void createItems() {
        this.readyItem = new ItemStack(Material.LIME_STAINED_GLASS_PANE, 1);
        List<String> readyLore = new ArrayList<>();
        readyLore.add(Message.TRADE_READY_LORE.get());

        ItemMeta readyItemMeta = readyItem.getItemMeta();
        readyItemMeta.setDisplayName(Message.TRADE_READY_TITLE.get());
        readyItemMeta.setLore(readyLore);
        readyItem.setItemMeta(readyItemMeta);

        this.notReadyItem = new ItemStack(Material.RED_STAINED_GLASS_PANE, 1);
        List<String> notReadyLore = new ArrayList<>();
        notReadyLore.add(Message.TRADE_NOT_READY_LORE.get());

        ItemMeta notReadyItemMeta = notReadyItem.getItemMeta();
        notReadyItemMeta.setDisplayName(Message.TRADE_NOT_READY_TITLE.get());
        notReadyItemMeta.setLore(notReadyLore);
        notReadyItem.setItemMeta(notReadyItemMeta);

        this.targetSlots = new HashSet<>();
        this.playerSlots = new HashSet<>();

        Collections.addAll(targetSlots, 0, 1, 2, 3, 9, 10, 11, 12, 18, 19, 20, 21, 27, 28, 29, 30);
        Collections.addAll(playerSlots, 5, 6, 7, 8, 14, 15, 16, 17, 23, 24, 25, 26, 32, 33, 34, 35);
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            return true;
        }
        Player player = (Player) sender;
        if (args.length != 1) {
            Message.TRADE_HELP.prefix().send(player);
            return true;
        }
        if (args[0].equals("accept")) {
            accept(player);
            return true;
        }
        String name = args[0];
        Player target = Bukkit.getPlayer(name);
        if (target == null) {
            Message.TRADE_PLAYER_OFFLINE.prefix().send(player);
            return true;
        }
        if (player == target) {
            Message.TRADE_OFFER_SELF.prefix().send(player);
            return true;
        }
        add(player, target);
        return true;
    }

    private void add(Player player, Player target) {
        if (requests.get(target) == player) {
            Message.TRADE_ALREADY_SENT.prefix().send(player);
            return;
        }
        requests.put(target, player);

        new BukkitRunnable() {
            public void run() {
                Player cachedTarget = requests.get(target);
                if (cachedTarget == player) {
                    requests.remove(target);
                }
            }
        }.runTaskLater(plugin, 1200L);

        Message.TRADE_OFFER_SENT.replaced("{target}", target.getDisplayName()).prefix().send(player);
        Message.TRADE_OFFER_SENT_TARGET.replaced("{player}", player.getDisplayName()).prefix().send(player);
    }

    private void accept(Player player) {
        Player from = requests.get(player);
        if (from == null) {
            Message.TRADE_NO_REQUESTS.prefix().send(player);
            return;
        }
        if (!from.isOnline()) {
            Message.TRADE_PLAYER_OFFLINE.prefix().send(player);
            return;
        }
        requests.remove(player);
        new Trade(plugin, player, from, targetSlots, playerSlots, readyItem, notReadyItem);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        requests.remove(event.getPlayer());
    }
}
