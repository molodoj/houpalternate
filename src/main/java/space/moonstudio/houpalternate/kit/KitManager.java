package space.moonstudio.houpalternate.kit;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import space.moonstudio.houpalternate.*;
import space.moonstudio.houpalternate.command.KitCommand;
import space.moonstudio.houpalternate.*;
import space.moonstudio.houpalternate.command.KitCommand;
import space.moonstudio.houpalternate.command.KitsCommand;
import space.moonstudio.houpalternate.util.ItemUtils;
import space.moonstudio.houpalternate.util.TimeUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Getter
public class KitManager implements Listener  {

    private Config cooldownConfig = new Config(new File(Main.getInstance().getDataFolder(), "cooldown.yml"));

    private KitsCommand kitsCommand;
    private boolean enable = false;

    private ItemStack bottleItem;
    private int bottleSlot;
    private Sound bottleSound;

    private ItemStack backItem;
    private int backSlot;
    private String backAction;
    private Sound backSound;

    private Sound cooldownSound;

    private List<Kit> kits;

    @SuppressWarnings("ConstantConditions")
    public void init() {
        Main main = Main.getInstance();

        Bukkit.getPluginManager().registerEvents(this, main);

        main.getCommand("kits").setExecutor(new KitsCommand(this));
        main.getCommand("kit").setExecutor(new KitCommand(this));

        Bukkit.getScheduler().runTaskTimer(main, () -> {
            for (Player player : Bukkit.getOnlinePlayers()) {
                InventoryView view = player.getOpenInventory();
                if (view != null && view.getTopInventory() != null && view.getTopInventory().getTitle().equals(Message.KIT_GUI_NAME.get())) {
                    this.updateGuiKitItems(player, view.getTopInventory());
                }
            }
        }, 20, 20);

        this.clearCooldownConfig(cooldownConfig);
    }

    private void clearCooldownConfig(Config cooldownConfig) {
        boolean save = false;
        long current = System.currentTimeMillis();
        for (String player : cooldownConfig.getMap().keySet()) {
            for (String key : cooldownConfig.getKeys(player)) {
                long nextUse = cooldownConfig.getNumber(player + "." + key).longValue();
                if (current > nextUse) {
                    cooldownConfig.set(player + "." + key, null);
                    save = true;
                }
            }
            if (cooldownConfig.getKeys(player).isEmpty()) {
                cooldownConfig.set(player, null);
            }
        }
        if (save) {
            cooldownConfig.save();
        }
    }

    public void reload(Config config) {
        enable = config.getOrSet("kitSystem", true);


        for (Metadata value : Metadata.getPlayers().values()) {
            value.setKitsGui(null);
        }

        cooldownSound = getSound(config, "cooldown-sound");

        bottleItem = ItemUtils.stringToItem(config.getOrSet("gui-kit-bottle-item", "{\"item\":{\"==\":\"org.bukkit.inventory.ItemStack\",\"v\":1631,\"type\":\"GLASS_BOTTLE\"}}"));
        bottleItem = ItemUtils.hideInfo(bottleItem);
        bottleSlot = config.getOrSetNumber("gui-kit-bottle-slot", 9 * 5 + 3).intValue();
        bottleSound = getSound(config, "gui-kit-bottle-sound");

        backItem = ItemUtils.stringToItem(config.getOrSet("gui-kit-back-item", "{\"item\":{\"==\":\"org.bukkit.inventory.ItemStack\",\"v\":1631,\"type\":\"PLAYER_HEAD\",\"meta\":{\"==\":\"ItemMeta\",\"meta-type\":\"SKULL\",\"internal\":\"H4sIAAAAAAAAAE2PzU6DQBhFP01MkPgYbkkYKAiLLowYO6SAUH46s2NgsAwDNhRM6XP5gLJ0d2/OuYurAqjwdOhmKT/H76aVXIF7XMOz0zCOqk2tIZdxbWMalcYYZ1rtOGXTWMiwTUsFdR2d+Ti1/PIIysSv0zzyiwoAdwo85KWcOfzyxdfp8aTXR19WC7bXnh50GWFxfsFDvrA3bON+5btXe7+4/1xrKgtLEtM/0SGeWZ/rezORfJegqs9+Io/KII0RTatrIDpE+lAEXryJPjKL9NikKe1C8WWFxZpFZ0TF+42IvCWilkQEOkmzhabdLeh9EXlJR1eGB+Q28Xa7PoA/zJmHcRwBAAA=\"}}}"));
        backItem = ItemUtils.hideInfo(backItem);
        backSlot = config.getOrSetNumber("gui-kit-back-slot", 9 * 5 + 5).intValue();
        backAction = config.getOrSet("gui-kit-back-action", "CLOSE");
        if (backAction.startsWith("/")) {
            backAction = backAction.substring(1);
        }
        backSound = getSound(config, "gui-kit-back-sound");

        if (!config.contains("Kits")) {
            config.setAndSave("Kits.kek.start", true);
            // остальное само сгенерирует при загрузке
        }

        kits = new ArrayList<>();
        for (String name : config.getKeys("Kits")) {
            try {
                List<ItemStack> items = new ArrayList<>();
                for (String itemData : config.getOrSet("Kits." + name + ".items", Collections.singletonList("{\"item\":{\"==\":\"org.bukkit.inventory.ItemStack\",\"v\":1631,\"type\":\"IRON_SHOVEL\"}}"))) {
                    try {
                        ItemStack itemStack = ItemUtils.stringToItem(itemData);
                        items.add(itemStack);
                    } catch (Exception e) {
                        Main.getInstance().getLogger().severe("Ошибка загрузки предмета в ките " + name + " " + itemData);
                        e.printStackTrace();
                    }
                }
                ItemStack icon = ItemUtils.stringToItem(config.getOrSet("Kits." + name + ".icon", "{\"item\":{\"==\":\"org.bukkit.inventory.ItemStack\",\"v\":1631,\"type\":\"IRON_SHOVEL\"}}"));
                icon = ItemUtils.hideInfo(icon);
                kits.add(new Kit(
                        name,
                        icon,
                        config.getOrSetNumber("Kits." + name + ".slot", 9 + 1).intValue(),
                        config.getOrSet("Kits." + name + ".start", false),
                        config.getOrSetNumber("Kits." + name + ".cooldown", 100).intValue(),
                        items,
                        getSound(config, "Kits." + name + ".sound")));
            } catch (Exception e) {
                Main.getInstance().getLogger().severe("Ошибка загрузки кита " + name);
                e.printStackTrace();
            }
        }

        this.clearCooldownConfig(cooldownConfig);
        cooldownConfig.save();
    }

    public static Sound getSound(Config config, String key) {
        String name = config.getOrSet(key, Sound.ENTITY_EXPERIENCE_ORB_PICKUP.name());
        try {
            return Sound.valueOf(name);
        } catch (Exception e) {
            Main.getInstance().getLogger().severe("Не найден звук " + name + ", используйте имена звуков из https://hub.spigotmc.org/javadocs/bukkit/org/bukkit/Sound.html");
            return Sound.ENTITY_EXPERIENCE_ORB_PICKUP;
        }
    }

    @SuppressWarnings("ConstantConditions")
    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onInventoryDragEvent(InventoryDragEvent event) {
        Inventory topInventory = event.getView().getTopInventory();
        if (topInventory != null) {
            if (topInventory.getTitle().equals(Message.KIT_GUI_NAME.get())) {
                event.setCancelled(true);
            }
        }
    }

    @SuppressWarnings("ConstantConditions")
    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onInventoryClickEvent(InventoryClickEvent event) {
        Inventory topInventory = event.getView().getTopInventory();
        if (topInventory != null && topInventory.getTitle().equals(Message.KIT_GUI_NAME.get())) {
            event.setCancelled(true);
            Player player = (Player) event.getWhoClicked();

            ItemStack item = event.getCurrentItem();
            if (item != null && !item.getType().equals(Material.AIR)) {
                if (event.getSlot() == backSlot) {
                    if (backAction.equalsIgnoreCase("CLOSE")) {
                        player.closeInventory();
                    } else {
                        Bukkit.dispatchCommand(player, backAction);
                    }
                    playSound(player, backSound);
                }

                if (event.getSlot() == bottleSlot) {
                    playSound(player, bottleSound);
                }

                for (Kit kit : kits) {
                    if (kit.getSlot() == event.getSlot()) {
                        if (!isAvailable(kit, player)) {
                            playSound(player, cooldownSound);
                        } else {
                            performGiveKit(player, kit);
                        }
                        break;
                    }
                }
            }
        }
    }

    @EventHandler
    public void on(InventoryOpenEvent event) {
        Inventory inventory = event.getInventory();
        if (inventory.getTitle().equals(Message.KIT_GUI_NAME.get())) {
            this.updateGuiKitItems((Player) event.getPlayer(), inventory);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void on(PlayerJoinEvent event) {
        if (!event.getPlayer().hasPlayedBefore()) {
            for (Kit kit : kits) {
                Try.ignore(() -> {
                    if (kit.isStart() && canAccess(kit, event.getPlayer())) {
                        performGiveKit(event.getPlayer(), kit);
                    }
                });
            }
        }
    }

    public void updateGuiKitItems(Player player, Inventory inventory) {
        long current = System.currentTimeMillis();

        for (Kit kit : this.getKits()) {
            boolean access = this.canAccess(kit, player);
            long nextUse = getNextUse(kit, player);

            StringBuilder builder = new StringBuilder();
            if (nextUse > current) {
                builder.append(Message.GUI_KIT_ITEM_NAME_NOT_AVAILABLE.get("{name}", kit.getName()));
            } else {
                builder.append(Message.GUI_KIT_ITEM_NAME_AVAILABLE.get("{name}", kit.getName()));
            }

            for (ItemStack item : kit.getItems()) {
                builder.append("\n").append(Message.GUI_KIT_LIST_ITEM.get(
                        "{item}", ItemUtils.getMinecraftName(item),
                        "{amount}", String.valueOf(item.getAmount())
                        ));
            }

            if (nextUse > current) {
                int waitSeconds = (int) ((nextUse - current) / 1000);
                builder.append("\n");
                builder.append(Message.KIT_LORE_DISABLE.get("{time}", TimeUtils.formatDateDiff(waitSeconds)));
            }

            inventory.setItem(kit.getSlot(),
                    ItemUtils.setText(kit.getItem().clone(),
                            builder.toString()
                    ));
        }
    }

    public void performGiveKit(Player player, Kit kit) {
        playSound(player, kit.getSound());
        ItemUtils.giveItemsOrDrops(player, kit.getItemsClone());
        player.sendMessage(Message.KIT_SELECT.get("{name}", kit.getName()));
        cooldownConfig.set(player.getName().toLowerCase() + "." + kit.getName().toLowerCase(), System.currentTimeMillis() + (kit.getCooldownSeconds() * 1000));
        player.closeInventory();
        Metadata.get(player).setKitsGui(null);
    }

    public static void playSound(Player player, Sound sound) {
        player.playSound(player.getEyeLocation(), sound, 1, 1);
    }

    public boolean canAccess(Kit kit, Player player) {
        return true;
    }

    public boolean isAvailable(Kit kit, Player player) {
        long nextUse = getNextUse(kit, player);
        if (nextUse == -1) {
            return true;
        }
        return System.currentTimeMillis() >= nextUse;
    }

    public long getNextUse(Kit kit, Player player) {
        Number number = cooldownConfig.getNumber(player.getName().toLowerCase() + "." + kit.getName().toLowerCase());
        if (number == null) {
            return -1;
        }
        return number.longValue();
    }
}
