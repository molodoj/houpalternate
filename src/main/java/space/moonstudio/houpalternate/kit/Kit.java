package space.moonstudio.houpalternate.kit;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.bukkit.Sound;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
public class Kit {
    private String name;
    private ItemStack item;
    private int slot;
    private boolean start;
    private int cooldownSeconds;
    private List<ItemStack> items;
    private Sound sound;

    public List<ItemStack> getItemsClone() {
        return items.stream()
                .map(ItemStack::clone)
                .collect(Collectors.toList());
    }
}
