package space.moonstudio.houpalternate;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import lombok.Getter;
import lombok.SneakyThrows;
import net.minecraft.server.v1_13_R2.ChatDeserializer;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import space.moonstudio.houpalternate.command.AdminHoupAlternateCommand;
import space.moonstudio.houpalternate.command.AnvilCommand;
import space.moonstudio.houpalternate.command.DispellCommand;
import space.moonstudio.houpalternate.command.ListCommand;
import space.moonstudio.houpalternate.invcleaner.InvCleanerManager;
import space.moonstudio.houpalternate.kit.KitManager;
import space.moonstudio.houpalternate.trade.TradeManager;
import space.moonstudio.houpalternate.util.TimeUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Главный класс плагина
 */
@Getter
public class Main extends JavaPlugin {

    private static Main instance;

    private int timeSeconds;
    private Config customConfig;
    private ListCommand listCommand;
    private KitManager kitManager;

    private Map<String, String> locale;

    public Main() {
        instance = this;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onEnable() {
        this.getCommand("houpmine").setExecutor(new AdminHoupAlternateCommand());
        this.getCommand("anvil").setExecutor(new AnvilCommand());
        this.getCommand("dispell").setExecutor(new DispellCommand());
        listCommand = new ListCommand();
        this.getCommand("list").setExecutor(listCommand);

        kitManager = new KitManager();
        kitManager.init();

        new TradeManager(this);

        Bukkit.getPluginManager().registerEvents(new Events(), this);

        this.reloadCustomConfig();

        new InvCleanerManager(this, customConfig);
    }

    @SneakyThrows
    public void reloadCustomConfig() {
        this.getLogger().info("Reload config...");
        customConfig = new Config(new File(this.getDataFolder(), "config.yml"));
        customConfig.setDescription(
                "/houpmine reload - перезагрузить этот конфиг, перезагружаются все параметры (право ha.admin)\n" +
                        "priority в list -> categories решает в какую категорию игрок попадет, если у него есть группы разных категорий\n" +
                        "/houpmine item - превратить предмет в текст (для конфига)\n" +
                        "используйте имена звук из https://hub.spigotmc.org/javadocs/bukkit/org/bukkit/Sound.html");

        if (customConfig.contains("messages")) { // он попросил удалить субкатегорию
            customConfig.setAndSave("messages", null);
        }

        Config messageConfig = Message.getConfig();
        messageConfig.reload();
        if (messageConfig.contains("messages")) { // он попросил удалить субкатегорию
            messageConfig.setAndSave("messages", null);
        }
        messageConfig.setDescription(
                "/houpmine reload - перезагрузить этот конфиг, перезагружаются все параметры (право ha.admin)");
        Message.clearCache();
        Message.getMessages().forEach(message -> message.init(messageConfig));
        TimeUtils.init();

        timeSeconds = customConfig.getOrSet("time", 15);

        listCommand.reloadConfig(customConfig);

        kitManager.reload(customConfig);

        this.reloadLocale();

        this.getLogger().info("Plugin successfully reload.");
    }

    private static final Pattern b = Pattern.compile("%(\\d+\\$)?[\\d\\.]*[df]");

    @SneakyThrows
    private void reloadLocale() {
        File localeFile = new File(this.getDataFolder(), "locale.json");
        if (!localeFile.exists()) {
            URL inputUrl = Main.class.getClassLoader().getResource("locale.json");
            FileUtils.copyURLToFile(inputUrl, localeFile);
        }

        try (InputStream var0 = new FileInputStream(localeFile)) {
            JsonElement var1 = (new Gson()).fromJson(new InputStreamReader(var0, StandardCharsets.UTF_8),
                                                     JsonElement.class);
            JsonObject var2 = ChatDeserializer.m(var1, "strings");
            Iterator var4 = var2.entrySet().iterator();

            locale = new HashMap<>();
            while (var4.hasNext()) {
                Map.Entry<String, JsonElement> entry = (Map.Entry) var4.next();
                String var5 = b.matcher(ChatDeserializer.a((JsonElement) entry.getValue(), (String) entry.getKey()))
                               .replaceAll("%$1s");
                this.locale.put(entry.getKey(), var5);
            }
        }
    }

    @Override
    public void onDisable() {
        kitManager.getCooldownConfig().save();
    }

    public static Main getInstance() {
        return instance;
    }
}
