package space.moonstudio.houpalternate;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.command.CommandSender;
import space.moonstudio.houpalternate.util.MessageUtils;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("NonAsciiCharacters")
public enum Message {

    // можно добавить сообщение, как enum, а можно пользоваться просто getOrDefault
    PREFIX("&6HoupAlternate &8| &f"),

    NO_ENCHANTED("&cЭтот предмет не содержит чары"),
    CONFIRM("&fЧтобы подтвердить выполнение команды, напишите &7/dispell &fповторно"),
    DISPELL("Чары с предмета были удалены"),
    NO_ITEM("Возьмите предмет в руку"),
    CONFIG_RELOAD("§aКонфиг перезагружен."),
    NO_PERMISSION_ADMIN("§cНет прав by command admin."),
    NO_PERMISSION_ANVIL("§cНет прав by command anvil."),
    NO_PERMISSION_KITS("§cНет прав by command kits."),
    NO_PERMISSION_DISPELL("§cНет прав by command dispell."),
    NO_PERMISSION_LIST("§cНет прав by command list."),
    NO_PERMISSION_HELP("§cНет прав by command help."),
    KIT_SYSTEM_DISABLED("§cСистема китов выключена."),
    HELP("&e▸Помощь по плагину\n" +
                 "/dispell - снятие зачарований\n" +
                 "/anvil - наковальня\n" +
                 "/help - вывести список команд"),
    HOUPMINE_ARGUMENT_NOT_FOUND("§сАргумент команды не найден, используйте /houpmine help"),
    HOUPMINE_HELP("§e===============[HoupMine]===============\n" +
                          "§3/houpmine reload §7- перезагрузить конфиг плагина" +
                          "§3/houpmine item §7- превратить предмет в текст (для конфига)\n" +
                          "§3/houpmine fromstring [текст] §7- превратить текст в предмет\n"),
    KIT_GUI_NAME("Выбор набора"),
    GUI_KITS_BOTTLE_TEXT("текст предмета\n" +
                                 "пузырька\n" +
                                 "изменить можно в конфиге"),
    KIT_GUI_BACK_TEXT(
            "§cНазад\n" +
                    "§7Нажмите, чтобы вернуться назад"),
    GUI_KIT_ITEM_NAME_AVAILABLE("&a{name}"),
    GUI_KIT_ITEM_NAME_NOT_AVAILABLE("&c{name}"),
    KIT_SELECT("&eВам был выдан набор §a{name}"),
    KIT_LORE_DISABLE("§cНабор будет доступен через §c{time}"),
    ENTER_KIT_NAME("&cУкажите имя кита /kit <name>, киты: {kits}"),
    ENTER_KIT_NAME_SEPARATOR(", "),

    TRADE_HELP("/trade help"),
    TRADE_INVENTORY_TITLE("Обмен {target}-{player}"),
    TRADE_ALREADY_SENT("Вы уже отправили обмен данному игроку"),
    TRADE_OFFER_SELF("Вы не можете так сделать"),
    TRADE_OFFER_SENT("Вы отправили обмен игроку {target}"),
    TRADE_OFFER_SENT_TARGET("Вы получили запрос на обмен от игрока {player}"),
    TRADE_NO_REQUESTS("У Вас нет запросов на обмен"),
    TRADE_PLAYER_OFFLINE("Игрок не на сервере"),
    TRADE_NOT_READY_TITLE("Не готов"),
    TRADE_NOT_READY_LORE("Данный игрок не готов к обмену"),
    TRADE_READY_TITLE("Готов"),
    TRADE_READY_LORE("Данный игрок готов к обмену §c{time}"),
    KIT_COMMAND_DISABLE("§cНабор будет доступен через §c{time}"),
    GUI_KIT_LIST_ITEM("&7 - {item} x {amount}"),

    ГОД("год"),
    ГОДА("года"),
    ЛЕТ("лет"),

    МЕСЯЦ("месяц"),
    МЕСЯЦА("месяца"),
    МЕСЯЦЕВ("месяцев"),

    ДЕНЬ("день"),
    ДНЯ("дня"),
    ДНЕЙ("дней"),

    ЧАС("час"),
    ЧАСА("часа"),
    ЧАСОВ("часов"),

    МИНУТА("минута"),
    МИНУТЫ("минуты"),
    МИНУТ("минут"),

    СЕКУНДА("секунда"),
    СЕКУНДЫ("секунды"),
    СЕКУНД("секунд"),

    INVCLEANER_NO_PERMISSION("no permission"),
    INVCLEANER_SUCCESS("success"),
    INVCLEANER_NO_PERMISSION_OTHER("no permission"),
    INVCLEANER_SUCCESS_OTHER("success other"),
    INVCLEANER_TARGET_OFFLINE("player offline"),
    INVCLEANER_USAGE("usage"),

    ;

    @Getter
    private static Config config = new Config(new File(Main.getInstance().getDataFolder(), "messages.yml"));

    @Getter
    private static List<Message> messages = Arrays.asList(values());

    @Getter
    private String defaultValue;
    private String value;

    Message(String defaultValue) {
        this.defaultValue = this.value = MessageUtils.colored(StringUtils.stripToEmpty(defaultValue));
    }

    public void init(Config config) {
        this.value = MessageUtils.colored(config.getOrSet(this.name(), defaultValue));
    }

    /**
     * @deprecated see {@link Message#replaced}
     */
    @Deprecated
    public String get(String... replaced) {
        return MessageUtils.replaced(this.value, replaced);
    }

    public String get() {
        return this.value;
    }

    public Message prefix() {
        this.value = PREFIX.get().concat(value);
        return this;
    }

    public Message replaced(String... replaced) {
        this.value = MessageUtils.replaced(value, replaced);
        return this;
    }

    public void send(CommandSender sender) {
        sender.sendMessage(value);
    }

    public static void clearCache() {
        cache.clear();
    }

    private static Map<String, String> cache = new HashMap<>();

    /**
     * Альтернатива без enum
     */
    public static String getOrDefault(String key, String defaultValue) {
        return cache.computeIfAbsent(key.toUpperCase(),
                                     innerKey -> MessageUtils.colored(config.getOrSet(innerKey, defaultValue)));
    }

    /**
     * Альтернатива без enum
     */
    public static String getOrDefault(String key, String defaultValue, String... replaced) {
        return MessageUtils.replaced(getOrDefault(key, defaultValue), replaced);
    }
}
